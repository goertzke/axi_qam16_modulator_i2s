`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/09/2024 04:04:03 PM
// Design Name: 
// Module Name: pac
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pac(
    input [7:0] phase,
    output wire [23:0] iout,
    output wire [23:0] qout
    );

wire [7:0] iphase;
wire [6:0] qindex, iindex;
wire qneg, ineg;
wire [22:0] qampl, iampl;

// LUT is just a quarter of a sine wave. Calculate correct index from phase
assign qindex = phase[6] ? (128 - phase[6:0]) : phase[6:0];
assign qneg = phase[7];
assign qampl = lut(qindex);
assign qout = qneg ? -qampl : qampl;

assign iphase = phase + 64;
assign iindex = iphase[6] ? (128 - iphase[6:0]) : iphase[6:0];
assign ineg = iphase[7];
assign iampl = lut(iindex);
assign iout = ineg ? -iampl : iampl;

function [22:0] lut(input [6:0] index);
case (index)
    7'h00: lut = 23'h000000;
    7'h01: lut = 23'h03242b;
    7'h02: lut = 23'h0647d9;
    7'h03: lut = 23'h096a90;
    7'h04: lut = 23'h0c8bd3;
    7'h05: lut = 23'h0fab27;
    7'h06: lut = 23'h12c810;
    7'h07: lut = 23'h15e214;
    7'h08: lut = 23'h18f8b8;
    7'h09: lut = 23'h1c0b82;
    7'h0a: lut = 23'h1f19f9;
    7'h0b: lut = 23'h2223a5;
    7'h0c: lut = 23'h25280c;
    7'h0d: lut = 23'h2826b9;
    7'h0e: lut = 23'h2b1f35;
    7'h0f: lut = 23'h2e110a;
    7'h10: lut = 23'h30fbc5;
    7'h11: lut = 23'h33def2;
    7'h12: lut = 23'h36ba20;
    7'h13: lut = 23'h398cdd;
    7'h14: lut = 23'h3c56ba;
    7'h15: lut = 23'h3f1749;
    7'h16: lut = 23'h41ce1e;
    7'h17: lut = 23'h447acd;
    7'h18: lut = 23'h471cec;
    7'h19: lut = 23'h49b415;
    7'h1a: lut = 23'h4c3fdf;
    7'h1b: lut = 23'h4ebfe8;
    7'h1c: lut = 23'h5133cc;
    7'h1d: lut = 23'h539b2a;
    7'h1e: lut = 23'h55f5a4;
    7'h1f: lut = 23'h5842dd;
    7'h20: lut = 23'h5a8279;
    7'h21: lut = 23'h5cb420;
    7'h22: lut = 23'h5ed77c;
    7'h23: lut = 23'h60ec37;
    7'h24: lut = 23'h62f201;
    7'h25: lut = 23'h64e888;
    7'h26: lut = 23'h66cf80;
    7'h27: lut = 23'h68a69e;
    7'h28: lut = 23'h6a6d98;
    7'h29: lut = 23'h6c2429;
    7'h2a: lut = 23'h6dca0c;
    7'h2b: lut = 23'h6f5f02;
    7'h2c: lut = 23'h70e2cb;
    7'h2d: lut = 23'h72552c;
    7'h2e: lut = 23'h73b5eb;
    7'h2f: lut = 23'h7504d2;
    7'h30: lut = 23'h7641ae;
    7'h31: lut = 23'h776c4e;
    7'h32: lut = 23'h788483;
    7'h33: lut = 23'h798a23;
    7'h34: lut = 23'h7a7d04;
    7'h35: lut = 23'h7b5d03;
    7'h36: lut = 23'h7c29fb;
    7'h37: lut = 23'h7ce3ce;
    7'h38: lut = 23'h7d8a5e;
    7'h39: lut = 23'h7e1d93;
    7'h3a: lut = 23'h7e9d55;
    7'h3b: lut = 23'h7f0991;
    7'h3c: lut = 23'h7f6236;
    7'h3d: lut = 23'h7fa736;
    7'h3e: lut = 23'h7fd887;
    7'h3f: lut = 23'h7ff621;
    7'h40: lut = 23'h7fffff;
    default: lut = 23'h000000;
endcase
endfunction

endmodule