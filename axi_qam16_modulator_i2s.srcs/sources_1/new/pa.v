`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/09/2024 04:04:03 PM
// Design Name: 
// Module Name: pa
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pa(
    input wire clk,
    input wire resetn,
    input wire [7:0] freq,
    output [7:0] phase
    );

reg [7:0] phase = 0;

always @(posedge clk) begin
    if (!resetn) begin
        phase <= 0;
    end
    phase <= phase + freq;
end
endmodule
