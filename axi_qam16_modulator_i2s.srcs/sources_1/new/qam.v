`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/10/2024 05:26:07 PM
// Design Name: 
// Module Name: qam
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module qam(
    input wire clk,
    input wire [23:0] I,
    input wire [23:0] Q,
    input wire fifo_empty,
    input wire [31:0] fifo_dout,
    output reg fifo_rd_en,
    output wire signed [23:0] dout,
    output wire [7:0] freq
    );

wire signed [20:0] I_div8;
wire signed [20:0] Q_div8;
reg signed [2:0] I_mult = 0;
reg signed [2:0] Q_mult = 0;

reg [3:0] sym = 0;

reg [7:0] counter = 0;
reg fifo_rd_en = 0;

localparam Idle = 2'b00,Sym1 = 2'b01,Sym2 = 2'b10,Ready = 2'b11;
reg [1:0] state = Idle;

wire [7:0] T;

assign freq = fifo_dout[31:24];
assign T = fifo_dout[23:16];

assign I_div8 = I[23:3];
assign Q_div8 = Q[23:3];

assign dout = I_mult * I_div8 + Q_mult * Q_div8;

always @(posedge clk) begin
    if (counter < T) begin
        counter <= counter + 1;
    end else begin
        counter <= 0;
    end
end

always @(posedge clk) begin
    case (state)
      Idle:
        begin
            if (!fifo_empty && counter == 0) begin
                fifo_rd_en <= 1;
                state <= Ready;
            end
        end
      Ready:
        begin
            fifo_rd_en <= 0;
            sym <= fifo_dout[3:0];
            state <= Sym1;
        end
      Sym1:
        begin
            if (counter == T) begin
                sym <= fifo_dout[7:4];
                state <= Sym2;
            end
        end
      Sym2:
        begin
            if (counter == T) begin
                if (!fifo_empty) begin
                    fifo_rd_en <= 1;
                    state <= Ready;
                end else begin
                    state <= Idle;
                end
            end
        end
    endcase
end

always @(posedge clk) begin
    if (state == Sym1 || state == Sym2) begin
        case (sym)
          4'b0000:
            begin
                I_mult <= -3;
                Q_mult <= 3;
            end
          4'b0001:
            begin
                I_mult <= -3;
                Q_mult <= 1;
            end
          4'b0010:
            begin
                I_mult <= -3;
                Q_mult <= -3;
            end
          4'b0011:
            begin
                I_mult <= -3;
                Q_mult <= -1;
            end
          4'b0100:
            begin
                I_mult <= -1;
                Q_mult <= 3;
            end
          4'b0101:
            begin
                I_mult <= -1;
                Q_mult <= 1;
            end
          4'b0110:
            begin
                I_mult <= -1;
                Q_mult <= -3;
            end
          4'b0111:
            begin
                I_mult <= -1;
                Q_mult <= -1;
            end
          4'b1000:
            begin
                I_mult <= 3;
                Q_mult <= 3;
            end
          4'b1001:
            begin
                I_mult <= 3;
                Q_mult <= 1;
            end
          4'b1010:
            begin
                I_mult <= 3;
                Q_mult <= -3;
            end
          4'b1011:
            begin
                I_mult <= 3;
                Q_mult <= -1;
            end
          4'b1100:
            begin
                I_mult <= 1;
                Q_mult <= 3;
            end
          4'b1101:
            begin
                I_mult <= 1;
                Q_mult <= 1;
            end
          4'b1110:
            begin
                I_mult <= 1;
                Q_mult <= -3;
            end
          4'b1111:
            begin
                I_mult <= 1;
                Q_mult <= -1;
            end
        endcase
    end else begin
        I_mult <= 0;
        Q_mult <= 0;
    end
end

endmodule
