`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/09/2024 04:04:03 PM
// Design Name: 
// Module Name: nco
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module nco(
    input clk,
    input resetn,
    input [7:0] freq,
    output [23:0] iout,
    output [23:0] qout
    );

wire [7:0] pa_out;
    
pa PA(
    .clk (clk),
    .resetn (resetn),
    .freq (freq),
    .phase (pa_out)
    );

pac PAC(
    .phase (pa_out),
    .iout (iout),
    .qout (qout)
    );
endmodule
