`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/07/2024 03:19:59 PM
// Design Name: 
// Module Name: i2s_controller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module i2s_controller(
    input wire mclk, // 12.288 MHz input clock
    input wire [23:0] din, // pbdat is sampled from here in every pblrc cycle
    output reg bclk = 0, // MCLK/4
    output reg pblrc = 0, // 48 kHz (MCLK/256)
    output reg pbdat = 0 // 500 Hz (MCLK/24576)
    );

    // Counter for generating pblrc and bclk
    reg [7:0] count_256 = 0;
    
    // Buffer for din to avoid timing issues
    reg [23:0] buffer = 0;

    // Which bit of buffer is currently being sent? (24 == not currently sending data)
    reg [4:0] index = 24;

    // Clock enable signals
    wire enable_bclk = (count_256[0] == 1'b1); // Enable bclk at MCLK/2
    wire enable_bit = (count_256[1:0] == 2'b11); // Send a bit
    wire enable_pblrc = (count_256[6:0] == 127); // Enable pblrc at MCLK/128
    wire enable_sample = (count_256 == 255); // Sample din to buffer

    // Counter
    always @(posedge mclk) begin
        // Counter
        count_256 <= count_256 + 1;
    end

    // Clocks and data sampling
    always @(posedge mclk) begin
        // Generate bclk (MCLK/4)
        if (enable_bclk) begin
            bclk <= ~bclk;
        end

        // Generate pblrc (MCLK/256)
        if (enable_pblrc) begin
            pblrc <= ~pblrc;
        end
        
        // Sample data (MCLK/256)
        if (enable_sample) begin
            buffer <= din;
        end
    end
    
    always @(posedge mclk) begin
        if (index < 24) begin
            // Sending data
            if (enable_bit) begin
                pbdat <= buffer[23-index];
                index <= index + 1;
            end
        end else begin
            if (enable_bit) begin
                pbdat <= 0;
            end
            if (enable_pblrc) begin
                index <= 0;
            end
        end
    end
endmodule