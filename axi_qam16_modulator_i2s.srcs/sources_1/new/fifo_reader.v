`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/10/2024 11:25:25 AM
// Design Name: 
// Module Name: fifo_reader
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fifo_reader(
    input wire [31:0] din,
    output wire [7:0] dout,
    output wire rd_enable
    );

assign rd_enable = 1'b1;
assign dout = din[7:0];
endmodule
