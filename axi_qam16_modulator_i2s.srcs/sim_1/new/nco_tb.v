`timescale 1us / 1ns
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/10/2024 09:37:05 AM
// Design Name: 
// Module Name: nco_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module nco_tb();

reg clk, resetn;
reg [7:0] freq;
wire [23:0] iout, qout;

nco UUT(
    .clk (clk),
    .resetn (resetn),
    .freq (freq),
    .iout (iout),
    .qout (qout)
    );

initial begin
    clk = 0;
    resetn = 1;
    freq = 0;
    #60
    freq = 1;
    #60
    resetn = 0;
    #10000
    freq = 13;
end

always #10 clk = ~clk;

endmodule
