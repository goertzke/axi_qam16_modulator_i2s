`timescale 1us / 1ns
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/09/2024 04:46:20 PM
// Design Name: 
// Module Name: pac_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pac_tb();

reg [7:0] phase;
wire [23:0] dout;

pac UUT(
    .phase (phase),
    .dout (dout)
    );

initial begin
    phase = 0;
end

always #20 phase = phase + 13;

endmodule
