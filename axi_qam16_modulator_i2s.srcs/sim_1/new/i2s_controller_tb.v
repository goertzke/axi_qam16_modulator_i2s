`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/10/2024 10:41:27 AM
// Design Name: 
// Module Name: i2s_controller_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module i2s_controller_tb();

reg mclk, resetn;
reg [7:0] freq, phase;
wire [23:0] din;
wire bclk, pblrc, pbdat;

i2s_controller UUT(
    .mclk (mclk),
    .din (din),
    .bclk (bclk),
    .pblrc (pblrc),
    .pbdat (pbdat)
    );

nco NCO(
    .clk (pblrc),
    .resetn (resetn),
    .freq (freq),
    .phase (phase),
    .dout (din)
    );

initial begin
    mclk = 0;
    resetn = 1;
    freq = 1;
    phase = 0;
end

// Generate mclock: 12.288 MHz
always #81 mclk = ~mclk;

endmodule
