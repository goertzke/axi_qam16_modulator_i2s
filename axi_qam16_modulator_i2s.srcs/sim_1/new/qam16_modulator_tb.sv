`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/09/2024 11:53:10 AM
// Design Name: 
// Module Name: qam16_modulator_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
import axi_vip_pkg::*;
import qam16_modulator_sim_axi_vip_0_0_pkg::*;

// Clocks and Reset
bit aclk = 0, aresetn = 1, mclk = 0;

// AXI4-Lite signals
xil_axi_resp_t resp;
bit[31:0] addr, data, base_addr = 32'h0000_0000;

module qam16_modulator_tb( );

qam16_modulator_sim_wrapper UUT(
    .aclk (aclk),
    .aresetn (aresetn),
    .mclk (mclk)
    );

// Generate the clock: 100 MHz
always #5ns aclk = ~aclk;

// Generate mclock: 12.288 MHz
always #81380ps mclk = ~mclk;

initial begin
    aresetn = 0;
    #100ns
    aresetn = 1;
end

qam16_modulator_sim_axi_vip_0_0_mst_t master_agent;

initial begin
    master_agent = new("master vip agent", UUT.qam16_modulator_sim_i.axi_vip_0.inst.IF);
    master_agent.start_master();
    
    wait (aresetn == 1'b1);
    
    #100ns
    data = 32'h01ff00f1;
    master_agent.AXI4LITE_WRITE_BURST(base_addr + addr, 0, data, resp);
    #100ns
    data = 32'h01ff00f2;
    master_agent.AXI4LITE_WRITE_BURST(base_addr + addr, 0, data, resp);
end

endmodule